# librerias necesarias
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.decomposition import FastICA, PCA
from scipy.io.wavfile import read, write
from IPython.display import Audio


def run_ubicaciones(dt, dtt, dtm):
    import matplotlib.pyplot as plt
    import math
    rec_max_x = math.ceil(np.amax(dt, axis=0)[0] * 1.15)
    rec_max_y = math.ceil(np.amax(dt, axis=0)[1] * 1.15)

    plt.style.use('seaborn')
    fig = plt.figure(figsize=(15, 10))
    ax = fig.gca()
    rectangle = plt.Rectangle((0, 0), rec_max_x, rec_max_y,
                              fc='purple', alpha=0.3)
    ax.add_patch(rectangle)

    i = 1
    for punto in dtt:
        circle = plt.Circle(punto, radius=0.015 * min(np.amax(dt, axis=0)), fc='green')
        ax.add_patch(circle)
        plt.text(punto[0], punto[1], '$s_{}(t)$'.format(i), fontsize=20)
        i += 1

    i = 1
    for punto in dtm:
        circle = plt.Circle(punto, radius=0.015 * min(np.amax(dt, axis=0)), fc='b')
        ax.add_patch(circle)
        plt.text(punto[0], punto[1], '$x_{}(t)$'.format(i), fontsize=20)
        i += 1
        for punto2 in dtt:
            plt.plot([punto2[0], punto[0]], [punto2[1], punto[1]],
                     "--", color="g", lw=1)

    ax.set_xticks(np.arange(0, rec_max_x * 1.05, 0.5))
    ax.set_yticks(np.arange(0, rec_max_y * 1.05, 0.5))

    plt.axis('scaled')
    plt.show();


t = np.linspace(0, 10, 441000)
frec = [1150, 440, 210]
Fs = 44100


def data_tono_puro(tono):
    return [np.sin(2 * np.pi * time * frec[tono - 1]) for time in t]


# vector puro
signals_pure = np.c_[data_tono_puro(1), data_tono_puro(2),
                     data_tono_puro(3)].astype(np.float)  # señales fuentes
signals_pure /= signals_pure.std(axis=0)  # datos estandarizados


def matriz_mezcla(dtt, dtm):
    intensidad = list()
    for u_mezcla in dtm:
        renglon = list()
        for u_tonop in dtt:
            renglon.append(1 / ((u_mezcla[0] - u_tonop[0]) ** 2 + (u_mezcla[1] - u_tonop[1]) ** 2))
        intensidad.append(renglon)
    matriz_mezcla = np.array(intensidad)
    return matriz_mezcla, (matriz_mezcla @ signals_pure.T).T


def graf_s(vector_senal, tono, segIni = 1, segFin = 1.02):
    import numpy as np
    locs = np.linspace(0, ((segFin - segIni) * 44100), 11)
    inicio = int(Fs * segIni)
    fin = int(Fs * segFin)
    fig, ax1 = plt.subplots(figsize=(12, 3))
    label = ['{} seg'.format(segIni), '', '', '', '', '{} seg'.format((segIni + segFin) / 2), '', '', '', '',
             '{} seg'.format(segFin)]
    ax1.plot(vector_senal.T[tono - 1][inicio:fin])
    plt.sca(ax1)
    plt.title('Mezcla')
    plt.ylabel('Amplitud')
    plt.xticks(locs, label)
    plt.show()
    return Audio(vector_senal.T[tono - 1], rate=Fs)


def tabla_mezclas(dtt, dtm):
    matriz, signals_mixes = matriz_mezcla(dtt, dtm)
    return pd.DataFrame(np.array([t, signals_mixes.T[0], signals_mixes.T[1], signals_mixes.T[2]]).T,
                        columns=['TIEMPO', 'VALOR MEZCLA 1', 'VALOR MEZCLA 2', 'VALOR MEZCLA 3']).set_index('TIEMPO')
